# XML to CSV

See also new.xml, new2csv.xsl and new.csv (different paths)

* Put following lines at top of .xml
	* `<?xml version="1.0" encoding="UTF-8" ?>`
	* `<?xml-stylesheet type="text/xsl" href="xml_2_csv.xsl"?>`
* Copy `xml_2_csv.xsl` to same folder
* Open xml and check columns have info
* Save output as .csv
* Open in Excel

## Source links

* Stackoverflow [xml-to-csv-using-xslt](http://stackoverflow.com/questions/365312/xml-to-csv-using-xslt)
* Quackit [xslt_example](http://www.quackit.com/xml/tutorial/xslt_example.cfm)
* Stackoverflow [xlst transposing-rows-to-columns](http://stackoverflow.com/questions/23951853/xslt-convert-xml-to-csv-transposing-rows-to-columns)
* [xml to csv converter](http://xmlgrid.net/xml2text.html)
* [Excel transpose](http://office.microsoft.com/en-gb/excel-help/switch-transpose-columns-and-rows-HP010224502.aspx)

## Used code samples

		<!-- src http://stackoverflow.com/questions/365312/xml-to-csv-using-xslt -->
		<!-- see http://www.quackit.com/xml/tutorial/xslt_example.cfm -->
		<xsl:stylesheet version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
		<xsl:output method="text" encoding="UTF-8"/>
		<!-- <Mailings>
			<Mailing><Fields><COLUMN><NAME>player_ref</NAME><VALUE>T01</VALUE></COLUMN><COLUMN><NAME>.../Mailing>
			<Mailing><Fields>...  -->		
		<xsl:template match="/">
			<xsl:for-each select="Mailings/Mailing/Fields/COLUMN[1]/NAME">
				<xsl:variable name="i" select="position()" />
					<xsl:for-each select="/Mailing/Fields/COLUMN">
						<xsl:value-of select="COLUMN[$i]/VALUE"/>
						<xsl:if test="position()!=last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
				<xsl:if test="position()!=last()">
					<xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:for-each>
			
		</xsl:template>

		</xsl:stylesheet>
		<!-- 
		http://stackoverflow.com/questions/23951853/xslt-convert-xml-to-csv-transposing-rows-to-columns 
		-->
		<!-- 
		<xsl:template match="tutorial">
		  <span class="tutorial-name"><xsl:value-of select="name"/></span>
		  <span class="tutorial-url"><xsl:value-of select="url"/></span>
		</xsl:template>
		-->
		//select first row, get column headers
		for-each select="Rows/Row[1]/cell

			<xsl:for-each select="Mailings/Mailing[1]/Fields/COLUMN/NAME">
				<xsl:variable name="i" select="position()" />
			<xsl:value-of select="/Mailings/Mailing[1]/Fields/COLUMN[$i]/NAME"/>
					<xsl:if test="position()!=last()">
								<xsl:text>,</xsl:text>
							</xsl:if>
			</xsl:for-each>
			<xsl:text>&#10;</xsl:text>


		//for each column
		<xsl:for-each select="Mailings/Mailing/Fields[1]/COLUMN/NAME">

		//use all other values
			 <xsl:variable name="i" select="position()" />
				   <xsl:for-each select="/Mailings/Mailing/Fields">
				
				<xsl:for-each select="/Mailing/Fields/COLUMN[1]">
					<xsl:variable name="i" select="position()" />            
						<xsl:value-of select="COLUMN[$i]/NAME"/>
						<xsl:for-each select="/Mailing/Fields/COLUMN">
							<xsl:value-of select="COLUMN[$i]/VALUE"/>
							<xsl:if test="position()!=last()">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:if test="position()!=last()">
						<xsl:text>&#10;</xsl:text>
					</xsl:if>
				</xsl:for-each>
		
		
				<xsl:template match="/">
				<xsl:for-each select="Rows/Row[1]/cell">
					<xsl:variable name="i" select="position()" />
						<xsl:for-each select="/Rows/Row">
							<xsl:value-of select="cell[$i]"/>
							<xsl:if test="position()!=last()">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:for-each>
					<xsl:if test="position()!=last()">
						<xsl:text>&#10;</xsl:text>
					</xsl:if>
				</xsl:for-each>
		</xsl:template>
