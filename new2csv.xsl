<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" encoding="UTF-8"/>
<xsl:template match="/">
	<xsl:for-each select="fields/row[1]/item">
		<xsl:variable name="i" select="position()" />
	<xsl:value-of select="/fields/row[1]/item[$i]/name"/>
			<xsl:if test="position()!=last()">
                        <xsl:text>,</xsl:text>
                    </xsl:if>
	</xsl:for-each>
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="fields/row">
		<xsl:variable name="k" select="position()" />
		<xsl:for-each select="/fields/row[$k]/item/value">
			<xsl:variable name="j" select="position()" />
    		<xsl:value-of select="/fields/row[$k]/item[$j]/value"/>
			<xsl:if test="position()!=last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
       	</xsl:for-each>            
        <xsl:if test="position()!=last()">
         <xsl:text>&#10;</xsl:text>
        </xsl:if>
	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
