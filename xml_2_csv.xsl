<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" encoding="UTF-8"/>
	
<xsl:template match="/">
	<!-- get field names -->
	<xsl:for-each select="Mailings/Mailing[1]/Fields/COLUMN">
		<xsl:variable name="i" select="position()" />
	<xsl:value-of select="/Mailings/Mailing[1]/Fields/COLUMN[$i]/NAME"/>
			<xsl:if test="position()!=last()">
            	<xsl:text>,</xsl:text>
            </xsl:if>
	</xsl:for-each>
	
	<xsl:text>&#10;</xsl:text><!-- end of line -->
	
	<!-- get values -->
	<xsl:for-each select="Mailings/Mailing">
		<xsl:variable name="k" select="position()" />
		
		<xsl:for-each select="/Mailings/Mailing[$k]/Fields/COLUMN/VALUE">
			<xsl:variable name="j" select="position()" />
    		<xsl:value-of select="/Mailings/Mailing[$k]/Fields/COLUMN[$j]/VALUE"/>
			<xsl:if test="position()!=last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
       	</xsl:for-each>     
		
        <xsl:if test="position()!=last()">
        	<xsl:text>&#10;</xsl:text>
        </xsl:if>
	</xsl:for-each>
	
</xsl:template>
	
</xsl:stylesheet>
